using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDataSaver : MonoBehaviour
{
    public Database _dataBase;


    void Start()
    {
        //������� ��� ������ Vector3 � ��������� ��� ����� �� ���� ������
        Vector3 positionFromDatabase = new Vector3(_dataBase.PositionX, _dataBase.PositionY, _dataBase.PositionZ);

        //��������� ������ �� �������, ��������� � ������� ���� �� ��
        transform.position = positionFromDatabase;
        transform.Rotate(_dataBase.RotationX, _dataBase.RotationY, _dataBase.RotationZ);

    }


    void Update()
    {
        //���������� ������� ������� � ���� ������
        _dataBase.PositionX = gameObject.transform.position.x;
        _dataBase.PositionY = gameObject.transform.position.y;
        _dataBase.PositionZ = gameObject.transform.position.z;

        _dataBase.RotationX = gameObject.transform.eulerAngles.x;
        _dataBase.RotationY = gameObject.transform.eulerAngles.y;
        _dataBase.RotationZ = gameObject.transform.eulerAngles.z;
    }
}
