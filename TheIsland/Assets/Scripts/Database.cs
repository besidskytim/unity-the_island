using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//��� ������ ��������� ��������� ����� ����� Database � ������� �� ��� ����
[CreateAssetMenu(fileName = "New Item Database", menuName = "Database/New Database")]

public class Database : ScriptableObject
{
    public float PositionX;
    public float PositionY;
    public float PositionZ;

    public float RotationX;
    public float RotationY;
    public float RotationZ;

}
